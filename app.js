const app = require('./src/index');

const port = process.env.port || 3000;

app.listen(port, () => {
  console.info(`running on http://localhost:${port}`);
});
