# Prueba técnica Node JS

Imaginemos que estamos desarrollando una red social de personas que adoran a los perros.
Esta red social ene algunas peculiaridades:
● Cuando un usuario se da de alta, en el formulario de registro, introduce los siguientes
datos: username, email y password. Además, también se adjunta al formulario
información de la localización (latud, longitud) y el idioma del navegador.
● Los usuarios se relacionan con otros por una relación de “amistad”, dando lugar al
listado de amigos.
● Los usuarios del hemisferio norte de la erra se guardan en nuestra base de datos,
mientras que los del hemisferio sur se guardan en un sistema de terceros con el que
nos comunicaremos vía API.
Dado este ejemplo se pide realizar las siguientes tareas:

1. Diseñar y desarrollar una API rest que implemente el CRUD (operaciones Create,
    Read, Update y Delete) para la endad usuario.
2. Diseñar una query que sea capaz de devolver el listado de amigos de un usuario
    determinado, y otra que devuelva el contador de amigos totales de un usuario
    determinado.

## Requisitos

```
● Uso de Node.js y framework Express.js
● Uso de base de datos relacional SQL para alojar los datos.
● Uso de lenguaje SQL (no usar ORM)
● La programación asíncrona debe realizarse mediante promesas o async/await.
● Entrada y salida del API en formato JSON
● Antes de cada peción se debe imprimir un log con alguna información sobre la
peción que se está realizando.
● Control de errores y excepciones.
● Buena organización y eslo de código intentando seguir un patrón de diseño en el
que la lógica y el modelo de datos sean independientes.
```
## Código de parda

Función que a parr de latud y longitud dice si el usuario está en el hemisferio norte o en el
sur :
```  javascript
const isSouthOrNorth = (latitude, longitude) => {
 return new Promise((resolve, reject) => {
   setTimeout(() => {
     if (latitude >= 0 && latitude <= 


&& longitude >= ­180 && longitude <= 180) {
       resolve('N');
     } else if (latitude < 0 && latitude >= ­
&& longitude >= ­180 && longitude <= 180){
       resolve('S');
     } else {
       reject(new Error('Bad values'));
     }
   }, 700);
 });
};

```
Llamada a la API de terceros para almacenar usuarios del hemisferio sur:

```  javascript
const processSouthern = (data) => {
 return new Promise((resolve, reject) => {
   setTimeout(() => {
     resolve();
   }, 1000);
 });
};
```
## Observaciones

En caso de duda o falta de especificaciones, es tarea del desarrollador tomar las decisiones
pernentes para la realización de la prueba. Estas decisiones deben ser jusficadas en un
fichero incluido junto a la prueba, o bien comentadas claramente en el código.


