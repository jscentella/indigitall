const user = require('../controllers/userController');

/**
 *
 * @param {Request} req
 * @param {Response} res
 */
const getAllUsers = async (req, res) => {
  try {
    let users = await user.getAllUsers();

    res.status(200).send({ users });
  } catch (err) {
    manageErrors(req, res, err);
  }
};
/**
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Error} error
 */
const manageErrors = (req, res, error) => {
  res.status(err.status || 500).send({ error });
};

module.exports = {
  getAllUsers,
};
