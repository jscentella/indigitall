const db = {
  host: process.env.host || 'db4free.net',
  database: process.env.db || 'indigitall_bd',
  user: process.env.db_user || 'indigitall_user',
  password: process.env.db_pass || '48e5f489',
  port: process.env.db_port || 3306,
};

module.exports = db;
