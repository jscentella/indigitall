const mysql = require('mysql');
const db = require('../config/config');

const connection = mysql.createPool({
  host: db.host,
  user: db.user,
  password: db.password,
  database: db.database,
  connectionLimit: 100,
});

module.exports = connection;
// con.connect(function(err) {
//   if (err) throw err;
//   console.log('Connected!');
// });
